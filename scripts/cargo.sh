#!/bin/sh

export CARGO_HOME=$1/target/cargo-home

if [[ $DEBUG = true ]]
then
    echo "DEBUG MODE"
    cargo build --features "gtk/v3_22" && cp $1/target/debug/priority $2
else
    echo "RELEASE MODE"
    cargo  build --release --features "gtk/v3_22" && cp $1/target/release/priority $2
fi 