use crate::widgets::window;

use gio::{self, prelude::*, SettingsExt};
use gtk::prelude::*;
use gtk::*;

use std::rc::Rc;

#[derive(Debug, Clone)]
pub(crate) struct App {
    instance: gtk::Application,
    window: gtk::ApplicationWindow,
    settings: gio::Settings,
}

impl App {
    pub(crate) fn new(app: &gtk::Application) -> Self {
        let settings = gio::Settings::new("com.gitlab.Priority");

        let window = window::new_window();

        window.set_application(app);

        window.show_all();

        // Set app name
        glib::set_application_name("priority");
        glib::set_prgname(Some("Priority"));

        window.connect_delete_event(|_, _| {
            main_quit();
            gtk::Inhibit(false)
        });



        App { window, settings, instance: app.clone() }
    }

    pub fn setup_dark_theme(&self) {
        let gtk_settings = gtk::Settings::get_default().unwrap();
        self.settings.bind(
            "dark-theme",
            &gtk_settings,
            "gtk-application-prefer-dark-theme",
            gio::SettingsBindFlags::DEFAULT,
        );
    }
}
