use gtk::{self, prelude::*};

#[derive(Clone, Debug)]
pub struct UI {
    pub builder: gtk::Builder,
}

impl UI {
    pub fn new() -> UI {
        // The order here is important because some ui file depends on others

        let builder = gtk::Builder::new();

        builder
            .add_from_resource("/com/gitlab/Priority/gtk/about.ui")
            .expect("Can't load ui file: about.ui");

        builder
            .add_from_resource("/com/gitlab/Priority/gtk/list_actions.ui")
            .expect("Can't load ui file: list_actions.ui");

        builder
            .add_from_resource("/com/gitlab/Priority/gtk/window.ui")
            .expect("Can't load ui file: window.ui");

        UI { builder }
    }
}
