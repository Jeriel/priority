use gdk;
use gtk::prelude::*;
use gtk::{AboutDialog, Application, ApplicationWindow, MenuItem, MenuItemExt};

use crate::uibuilder;

use crate::widgets::{card_row, list_actions, list_card};

pub fn new_window() -> ApplicationWindow {

    // Add style provider
    let provider = gtk::CssProvider::new();
    provider.load_from_resource("com/gitlab/Priority/app.css");
    gtk::StyleContext::add_provider_for_screen(
        &gdk::Screen::get_default().expect("Error initializing gtk css provider."),
        &provider,
        gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
    );

    let ui = uibuilder::UI::new();

    let window: ApplicationWindow = ui
        .builder
        .get_object("app_window")
        .expect("Application window could not be loaded");

    let mnu_about: MenuItem = ui
        .builder
        .get_object("about_menu")
        .expect("about_menu not found");

    {
        let window = window.clone();
        mnu_about.connect_activate(move |_| {
            show_about(&window);
        });
    }

    let btn_add_list: gtk::Button = ui
        .builder
        .get_object("add-list-button")
        .expect("add-list-button not found");

    btn_add_list.connect_clicked(move |_| {
        let board_list: gtk::Box = ui
            .builder
            .get_object("board_lists")
            .expect("board_lists not found");

        let btn_box: gtk::ButtonBox = ui
            .builder
            .get_object("button_box_add_list")
            .expect("button_box_add_list not found");

        let position = board_list.get_child_position(&btn_box);

        let mut list = list_card::ListCard::new();

        list.lst_board.set_margin_left(10);

        board_list.pack_start(&list.lst_board, false, false, 5);
        board_list.set_child_packing(&list.lst_board, false, true, 5, gtk::PackType::Start);

        board_list.set_child_position(&btn_box, position + 1);

        let popover = gtk::Popover::new(&list.list_action);

        let list_action = list_actions::ListActions::new(popover, &mut list.name_list);

        list.list_action.set_popover(&list_action.popover);

        list.btn_add_card.connect_clicked(clone!(list => move |_| {
            let card_row = card_row::CardRow::new("Test");
            list.list_cards.add(&card_row.row);
            list.list_cards.show_all();
            println!("Card Added!");
        }));

        println!("List Added!");
    });

    return window;
}

fn show_about(window: &ApplicationWindow) {
    let ui = uibuilder::UI::new();

    let about_dialog: AboutDialog = ui
        .builder
        .get_object("about_dialog")
        .expect("about_dialog not found");
    about_dialog.set_modal(true);
    about_dialog.set_transient_for(window);
    about_dialog.show();
}
