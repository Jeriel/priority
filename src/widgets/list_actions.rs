use gtk::prelude::*;

pub struct ListActions {
    pub popover: gtk::Popover,
    action_box: gtk::Box,
    title: gtk::Label,
    actions_stack: gtk::Stack,
    menu_box: gtk::Box,
    btn_add_card: gtk::Button,
    btn_copy_list: gtk::Button,
    btn_move_list: gtk::Button,
    btn_sort_by: gtk::Button,
    btn_move_card: gtk::Button,
    btn_archive_card: gtk::Button,
    btn_archive_list: gtk::Button,
    rename_box: gtk::Box,
    txt_rename: gtk::Entry,
    btn_rename: gtk::Button,
}

impl ListActions {
    pub fn new(popover: gtk::Popover, name_list: &mut gtk::Label) -> ListActions {
        let popover = popover;

        let action_box = gtk::Box::new(gtk::Orientation::Vertical, 0);

        let title = gtk::Label::new("List Actions");

        action_box.add(&title);

        let separator = gtk::Separator::new(gtk::Orientation::Horizontal);

        action_box.add(&separator);

        popover.add(&action_box);

        let actions_stack = gtk::Stack::new();

        let menu_box = gtk::Box::new(gtk::Orientation::Vertical, 0);

        let btn_add_card = gtk::Button::new_with_label("Add Card");

        menu_box.add(&btn_add_card);

        let btn_copy_list = gtk::Button::new_with_label("Copy List");

        menu_box.add(&btn_copy_list);

        let btn_move_list = gtk::Button::new_with_label("Move List");

        menu_box.add(&btn_move_list);

        let separator = gtk::Separator::new(gtk::Orientation::Horizontal);

        menu_box.add(&separator);

        let btn_sort_by = gtk::Button::new_with_label("Sort By");

        menu_box.add(&btn_sort_by);

        let separator = gtk::Separator::new(gtk::Orientation::Horizontal);

        menu_box.add(&separator);

        let btn_move_card = gtk::Button::new_with_label("Move All Cards in This List");

        menu_box.add(&btn_move_card);

        let btn_archive_card = gtk::Button::new_with_label("Archive All Cards in This List");

        menu_box.add(&btn_archive_card);

        let separator = gtk::Separator::new(gtk::Orientation::Horizontal);

        menu_box.add(&separator);

        let btn_archive_list = gtk::Button::new_with_label("Archive This List");

        menu_box.add(&btn_archive_list);

        let rename_box = gtk::Box::new(gtk::Orientation::Horizontal, 0);

        let txt_rename = gtk::Entry::new();

        rename_box.add(&txt_rename);

        let btn_rename = gtk::Button::new_with_label("Rename");

        rename_box.add(&btn_rename);

        rename_box.show_all();

        menu_box.add(&rename_box);

        menu_box.show_all();

        actions_stack.add(&menu_box);

        actions_stack.set_visible_child(&menu_box);

        actions_stack.show_all();

        action_box.add(&menu_box);

        action_box.add(&actions_stack);

        popover.show_all();

        btn_rename.connect_clicked(clone!(txt_rename, name_list => move |_| {
            name_list.set_text(&txt_rename.get_text().unwrap());
        }));

        ListActions {
            popover,
            action_box,
            title,
            actions_stack,
            menu_box,
            btn_add_card,
            btn_copy_list,
            btn_move_list,
            btn_sort_by,
            btn_move_card,
            btn_archive_card,
            btn_archive_list,
            rename_box,
            txt_rename,
            btn_rename,
        }
    }
}
