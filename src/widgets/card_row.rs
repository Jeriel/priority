use gtk::prelude::*;

pub struct CardRow {
    pub row: gtk::ListBoxRow,
    card_box: gtk::Box,
    img: gtk::Image,
    title: gtk::Label,
}

impl CardRow {
    pub fn new(name: &str) -> CardRow {
        let row = gtk::ListBoxRow::new();

        row.get_style_context().map(|c| c.add_class("card"));

        let card_box = gtk::Box::new(gtk::Orientation::Vertical, 0);

        let img = gtk::Image::new_from_icon_name("application-images", 16);

        img.set_pixel_size(100);

        card_box.add(&img);

        let title = gtk::Label::new(name);

        card_box.add(&title);

        row.add(&card_box);

        row.show_all();

        CardRow {
            row,
            card_box,
            img,
            title,
        }
    }
}
