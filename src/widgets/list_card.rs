use gtk::prelude::*;
use gtk::WidgetExt;

#[derive(Clone, Debug)]
pub struct ListCard {
    pub lst_board: gtk::Box,
    header: gtk::ButtonBox,
    pub name_list: gtk::Label,
    pub list_action: gtk::MenuButton,
    settings_img: gtk::Image,
    scrolled_window: gtk::ScrolledWindow,
    viewport: gtk::Viewport,
    pub list_cards: gtk::ListBox,
    pub btn_add_card: gtk::Button,
}

impl ListCard {
    pub fn new() -> ListCard {
        let lst_board = gtk::Box::new(gtk::Orientation::Vertical, 5);

        let header = gtk::ButtonBox::new(gtk::Orientation::Horizontal);

        lst_board.add(&header);

        let name_list = gtk::Label::new("Name List 😎");
        name_list.set_halign(gtk::Align::Start);

        header.add(&name_list);

        let list_action = gtk::MenuButton::new();
        list_action.set_halign(gtk::Align::End);
        let settings_img = gtk::Image::new_from_icon_name("application-menu-symbolic", 16);
        settings_img.set_parent(&list_action);

        header.add(&list_action);

        let scrolled_window = gtk::ScrolledWindow::new(None, None);
        scrolled_window.set_propagate_natural_width(true);

        scrolled_window.set_min_content_width(250);
        scrolled_window.set_vexpand_set(true);
        scrolled_window.set_vexpand(true);

        lst_board.add(&scrolled_window);

        let viewport = gtk::Viewport::new(None, None);

        scrolled_window.add_with_viewport(&viewport);

        let list_cards = gtk::ListBox::new();

        viewport.add(&list_cards);

        let btn_add_card = gtk::Button::new_with_label("Add another card");

        btn_add_card.set_margin_bottom(5);

        lst_board.add(&btn_add_card);

        lst_board.set_child_packing(&btn_add_card, false, true, 10, gtk::PackType::Start);

        lst_board.show_all();

        ListCard {
            lst_board,
            header,
            name_list,
            list_action,
            settings_img,
            scrolled_window,
            viewport,
            list_cards,
            btn_add_card,
        }
    }
}
