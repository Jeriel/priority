use gio::prelude::*;
use std::env::args;

mod static_resource;
mod uibuilder;
#[macro_use]
mod util;
mod app;
mod widgets;

use crate::app::App;

fn main() {
    gtk::init().expect("Error initialising gtk");

    static_resource::init().expect("Error loading static resource");

    let application = gtk::Application::new("com.gitlab.Priority", gio::ApplicationFlags::empty())
        .expect("Initialization failed...");

    application.set_property_resource_base_path(Some("/org/gnome/Fractal"));

    let app = App::new(&application);
    app.setup_dark_theme();

    application.run(&args().collect::<Vec<_>>());
    gtk::main();
}
